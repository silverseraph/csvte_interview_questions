local stack = assert(require'stack')
local entry = assert(require'entry')

local ramdisk = {}

ramdisk.__index = ramdisk

ramdisk.new = function()
    local self = {}

    setmetatable(self, ramdisk)

    self.root = entry.new('dir')

    return self
end

local function path_tok(path, save_last)
    local t = {}

    local save

    if save_last == nil then
        save = false
    else
        save = save_last
    end

    if path:sub(1,1) ~= '/' then
        error("Path, " .. path .. " , is improperly formatted, and must be an abslute path.")
    end

    for tok in string.gmatch(path, '(%w+)') do
        table.insert(t, tok)
    end

    local s = stack.new()

    if save then
        for i = 1,(#t-1) do
            s:push(t[(#t-i)])
        end

        return s, t[#t]
    else
        for i = 1,#t do
            s:push(t[(#t-i)+1])
        end

        return s, nil
    end
end

local goget = function(self, path, leave_last)
    local d_stack, last = path_tok(path, leave_last)

    local dir = self.root

    --print('Trying to goget "' .. path .. "'. Beginning at root.")

    while true do
        local cd_dir = d_stack:pop()
        --print('Trying to cd to "' .. cd_dir .. '".')

        if cd_dir == nil then
            break
        else
            if not dir:is_dir() then
                error("Cannot 'cd' into a non-directory on '" .. cd_dir .. "' in '" .. path .. "'.")
            else
                dir = dir:cd(cd_dir)
            end
        end
    end

    return dir, last
end

ramdisk.path_tok = path_tok

ramdisk.goget = goget

ramdisk.ls = function(self, path)
    --[[
    --  path - the path of the directory to ls out
    --]]

    --print("ls of '" .. path .. "'")

    local entry = goget(self, path, false)

    entry:ls()
end

ramdisk.touch = function(self, path)
    --[[
    --  path - the path of the file to create or update the time
    --]]

    --print("touching '" .. path .. "'")

    local entry, name = goget(self, path, true)

    entry:touch(name)
end

ramdisk.mkdir = function(self, path)
    --[[
    --  path - the path of the directory to make
    --]]

    --print("mkdir'ing at '" .. path .. "'")

    local entry, name = goget(self, path, true)

    entry:mkdir(name)
end

ramdisk.cat = function(self, path)
    --[[
    --  path - the path of the file to write to stdout
    --]]

    --print("catting '" .. path .. "'.")

    local entry, name = goget(self, path, true)

    entry = entry:open(name)

    entry:cat()
end

ramdisk.find = function(self, needle, start_dir)
    --[[
    --  needle - the needle string for the whole ramdisk
    --]]

    if start_dir == nil then
        start_dir = '/'
    end

    local entry, name = goget(self, start_dir, false)

    if start_dir:sub(#start_dir, #start_dir) then
        --entry:search(needle, start_dir:sub(1, #start_dir-1))
        entry:search(start_dir:sub(1, #start_dir-1), needle)
    else
        --entry:search(needle, start_dir)
        entry:search(start_dir, needle)
    end
end

ramdisk.copy = function(self, path_a, path_b)
    --[[
    --  path_a - absolute path to path_a
    --  path_b - absolute path to path_b
    --]]
    
    local ea = goget(self, path_a, false)

    local eb, name = goget(self, path_b, true)

    if eb:has(name) then
        error('Refusing to overwrite ' .. path_b .. '.  The destination must be empty.')
    end

    if ea:is_dir() then
        eb:mkdir(name)
        eb = goget(self, path_b, false)
        ea:rcp(eb, eb) --eb as passed as the root of the coppied section
        --[[
        --  This helps to avoid problems where 'cp /home /home/bin' would 
        --  cause you to copy the contents of home, which would create a copy of home at bin, and then recurse into it as the contents of home
        --]]
    else
        eb:touch(name)
        eb.contents[name].contents = eb.contents
    end
end

ramdisk.write = function(self, path, data, append)
    --[[
    --  path - absolute path
    --  data - the data to write
    --  append - bool for 'do append'
    --]]

    local do_append = false

    if append then
        do_append = true
    end

    local entry, name = goget(self, path, true)

    if not entry:has(name) then
        entry:touch(name)
    end

    entry = entry.contents[name]

    entry:write(data, do_append)
end

ramdisk.tree = function(self)
    self.root:rls('')
end

ramdisk.save = function(self, name)
    local json = assert(require'json')

    local fd = io.open(name, 'w')

    fd:write(json.encode(self.root))
end

ramdisk.load = function(self, name)
    local json = assert(require'json')

    local fd = io.open(name, 'r')

    local str = fd:read'*all'

    self.root = json.decode(str)
end

return ramdisk
