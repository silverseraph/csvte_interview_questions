local stack = {}

stack.__index = stack

stack.new = function()
    local self = {}

    self.index = 0
    self.store = {}

    setmetatable(self, stack)

    return self
end

stack.push = function(self, item)
    self.index = self.index + 1
    self.store[self.index] = item
    return self
end

stack.pop = function(self)
    if self.index == 0 then
        return nil
    end
    local tmp = self.store[self.index]
    self.store[self.index] = nil
    self.index = self.index - 1
    return tmp
end

stack.clear = function(self)
    while self.index > 0 do
        self.store[self.index] = nil
        self.index = self.index - 1
    end
end

stack.peek = function(self)
    return self.store[self.index]
end

stack.iter = function(self)
    local me = self
    return function()
        return me:pop()
    end
end

stack.__len = function(self)
    return self.index
end

stack.__tostring = function(self)
    local t = {}
    for i = 1,self.index do
        local typ = type(self.store[i])
        if typ == 'string' or typ == 'number' or self.store[i].__tostring ~= nil then
            table.insert(t, tostring(self.store[i]))
        else
            table.insert(t, typ)
        end
    end
    return '[' .. table.concat(t, ',') .. ']'
end

stack.__concat = stack.push

stack.__unm = stack.pop

return stack
