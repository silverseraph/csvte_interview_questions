local function printf(s, ...)
    return io.write(s:format(...))
end

local function test(name, func, expected_status)
    printf('Starting test[%s]\n', name);

    local status, err = pcall(func)
    if status == expected_status then
        printf("test[%s]: pass\n", name)
    else
        printf("test[%s]: fail - %s\n", name, tostring(err))
    end

    print()
end

local function ls_root()
    local ramdisk = assert(require'ramdisk')

    local rd = ramdisk.new()

    rd:ls('/')
end

local function ls_bin()
    local ramdisk = assert(require'ramdisk')

    local rd = ramdisk.new()

    rd:ls('/bin')
end

local function ls_home()
    local ramdisk = assert(require'ramdisk')

    local rd = ramdisk.new()

    rd:mkdir('/home')
    rd:mkdir('/home/andrew')
    rd:mkdir('/home/benton')

    rd:ls('/home')
end

local function make_home()
    local ramdisk = assert(require'ramdisk')

    local rd = ramdisk.new()

    rd:mkdir('/home')
    rd:mkdir('/home/andrew')
    rd:mkdir('/home/benton')
    rd:mkdir('/home/name')
    rd:write('/home/andrew/bash', 'bash info')
    rd:write('/home/benton/bash', 'more bash info')
    rd:write('/home/info', 'home directory info')
    rd:write('/home/andrew/name', 'andrew')
    rd:write('/home/benton/name', 'benton')

    return rd
end

_G.make_home = make_home

local function mkdir_home()
    local ramdisk = assert(require'ramdisk')

    local rd = ramdisk.new()

    rd:mkdir('/home')
    rd:mkdir('/home/andrew')
    rd:mkdir('/home/benton')

    rd:ls('/home')

    if rd.root.contents.home.contents.andrew == nil then
        error('Failed to find the /home/andrew directory')
    end

    if rd.root.contents.home.contents.benton == nil then
        error('Failed to find the /home/benton directory')
    end
end

local function find_name()
    local rd = make_home()

    rd:find('name')
end

local function cat_andrew_bash()
    local rd = make_home()

    rd:cat('/home/andrew/bash')
end

local function write_andrew_test()
    local rd = make_home()

    rd:write('/home/andrew/test', 'this is test data.')

    assert(rd.root.contents.home.contents.andrew.contents.test.contents == 'this is test data.')
end

local function write_append_andrew_test()
    local rd = make_home()

    rd:write('/home/andrew/test', 'this is test data.\n')
    rd:write('/home/andrew/test', 'this is more test data.\n', true)

    assert(rd.root.contents.home.contents.andrew.contents.test.contents == 'this is test data.\nthis is more test data.\n')
end

local function write_overwrite_andrew_test()
    local rd = make_home()

    rd:write('/home/andrew/test', 'this is test data.\n')
    rd:write('/home/andrew/test', 'this is now my test data.\n', false)

    assert(rd.root.contents.home.contents.andrew.contents.test.contents == 'this is now my test data.\n')
end

local function touch_test()
    local rd = make_home()

    rd:touch('/test')

    assert(rd.root.contents.test ~= nil)
    assert(rd.root.contents.test.type == 'file')
    assert(rd.root.contents.test.contents == '')
end

local function touch_fail()
    local rd = make_home()

    rd:touch('/home/andrew')
end

local function copy_test()
    local rd = make_home()

    rd:copy('/home', '/bin')

    print('ls of /home')
    rd:ls('/home')
    print()
    print('ls of /bin')
    rd:ls('/bin')

    assert(rd.root.contents.home.contents.andrew.contents.bash.contents == rd.root.contents.bin.contents.andrew.contents.bash.contents)
    assert(rd.root.contents.home.contents.benton.contents.name.contents == rd.root.contents.bin.contents.benton.contents.name.contents)
end

local function main()
    --ls tests
    test('ls - /', ls_root, true)
    test('ls - /bin', ls_bin, false)
    test('ls - home', ls_home, true)

    --touch tests
    test('touch - /test', touch_test, true)
    test('touch - /home/andrew', touch_fail, false)

    --mkdir tests
    test('mkdir - home', mkdir_home, true)

    --write tests
    test('write - test', write_andrew_test, true)
    test('write - append', write_append_andrew_test, true)
    test('write - overwrite', write_overwrite_andrew_test, true)

    --cat tests
    test('cat - /home/andrew/bash', cat_andrew_bash, true)

    --copy tests
    test('copy - /home -> /bin', copy_test, true)

    --find tests
    test('find - name', find_name, true)
end

main()
