local entry = {}

entry.__index = entry

entry.new = function(entry_type)
    local self = {}

    if entry_type == 'file' then
        self.type = 'file'
        self.contents = ''
    elseif entry_type == 'dir' or entry_type == 'directory' then
        self.type = 'dir'
        self.contents = {}
    else
        error("An entry to the file table must be entered as either 'file' or 'dir'.")
    end

    self.time = os.time()

    setmetatable(self, entry)

    return self
end

entry.is_dir = function(self)
    if self.type == 'dir' or self.type == 'directory' then
        return true
    else
        return false
    end
end

entry.write = function(self, data, append)
    if self:is_dir() then
        error("Cannot write to a directory.  Must write to a file.")
    else
        if append then
            self.contents = self.contents .. data
        else
            self.contents = data
        end
    end
end

entry.cat = function(self)
    if self:is_dir() then
        error("Cannot 'cat' a directory.  Must 'cat' a file.")
    else
        print(self.contents)
    end
end

entry.touch = function(self, name)
    if not self:is_dir() then
        error("Cannot create file under a non-directory. Creation of file '" .. name .. "' failed.")
    else
        if self:has(name) then
            if self.contents[name]:is_dir() then
                error('Cannot create a file.  A directory is already in place.')
            else
                --print('A file is already in place at this location.')
                self.contents[name].time = os.time()
            end
        end
        self.contents[name] = entry.new('file')
    end
end

entry.mkdir = function(self, name)
    if not self:is_dir() then
        error("Cannot create file under a non-directory. Creation of file '" .. name .. "' failed.")
    else
        if self:has(name) then
            if not self.contents[name]:is_dir() then
                error('Cannot create a directory.  A file is in place.')
            else
                print('A directory is already in place at this location.')
            end
        else
            self.time = os.time()
            self.contents[name] = entry.new('dir')
        end
    end
end

entry.cd = function(self, name)
    if not self:is_dir() then
        error("Cannot cd from a non-directory.");
    else
        if self.contents[name] ~= nil then
            if not self.contents[name]:is_dir() then
                error("Cannot cd into a file: '" .. name .. "'.")
            else
                return self.contents[name]
            end
        else
            error("No entry by the name of '" .. name .. "' exists.")
        end
    end
end

entry.open = function(self, name)
    if not self:is_dir() then
        error("Cannot open from a non-directory.")
    else
        if self.contents[name] ~= nil then
            if self.contents[name]:is_dir() then
                error("Cannot open a non-file: '" .. name .. "'.")
            else
                return self.contents[name]
            end
        else
            error("No entry by the name of '" .. name .. "' exists.")
        end
    end
end

entry.ls = function(self)
    if not self:is_dir() then
        error("Cannot ls in a non-directory.")
    else
        for k,v in pairs(self.contents) do
            print(("%s %s"):format(k, tostring(v)))
        end
    end
end

entry.search = function(self, running_path, needle)
    for k,v in pairs(self.contents) do
        if k == needle then
            print(running_path .. '/' .. needle)
        end
        if v:is_dir() then
            v:search(running_path .. '/' .. k, needle)
        end
    end
end

entry.has = function(self, name, type)
    if self:is_dir() then
        if self.contents[name] ~= nil then
            if type ~= nil then
                return self.contents[name].type == type
            else
                return true
            end
        else
            return false
        end
    else
        error("Cannot for containment in a non-directory.")
    end
end

entry.rcp = function(self, dst, root)
    for k,v in pairs(self.contents) do
        if v ~= root then --ensure that you don't end up in infinite loop hell
            if v:is_dir() then
                dst:mkdir(k)
                v:rcp(dst.contents[k], root)
            else
                dst:touch(k)
                dst.contents[k].contents = v.contents
            end
        end
    end
end

entry.rls = function(self, cen)
    if self:is_dir() then
        for k,v in pairs(self.contents) do

            if v:is_dir() then
                print(cen .. '/' .. k .. '/')
                v:rls(cen .. '/' .. k)
            else
                print(cen .. '/' .. k)
            end
        end
    else
        error("Cannot ls on a non-directory.")
    end
end

local fmt_string = '<%s> %d [%s]'

entry.__tostring = function(self)
    return fmt_string:format(self.type, #self.contents, os.date("%x", self.time))
end

return entry
